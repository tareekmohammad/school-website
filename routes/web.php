<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('', 'App\Http\Controllers\HomeController@index');
Route::get('contact', 'App\Http\Controllers\ContactController@index');
Route::get('register', 'App\Http\Controllers\RegisterController@index');
Route::get('blog', 'App\Http\Controllers\BlogController@index');
Route::get('admissionenquiry', 'App\Http\Controllers\AdmissionenquiryController@index');
Route::get('news', 'App\Http\Controllers\NewsController@index');
Route::get('blogdetail', 'App\Http\Controllers\BlogdetailController@index');
Route::get('picture', 'App\Http\Controllers\PictureController@index');
Route::get('video', 'App\Http\Controllers\VideoController@index');
Route::get('kitlist', 'App\Http\Controllers\KitlistController@index');
Route::get('ourvision', 'App\Http\Controllers\OurvisionController@index');

