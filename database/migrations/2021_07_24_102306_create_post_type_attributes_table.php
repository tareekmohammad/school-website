<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostTypeAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_type_attributes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_type_id')->unsigned()->nullable();
            $table->foreign('post_type_id')->references('id')->on('post_type_attributes')->onDelete('cascade');
            $table->enum('category_include', ['yes','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_type_attributes');
    }
}
