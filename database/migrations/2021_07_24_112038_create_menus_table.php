<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
             $table->enum('type', ['external','post_type']);
             $table->bigInteger('post_type_id')->unsigned()->nullable();
             $table->foreign('post_type_id')->references('id')->on('menus')->onDelete('cascade');
             $table->bigInteger('posts')->unsigned()->nullable();
             $table->foreign('posts')->references('id')->on('menus')->onDelete('cascade');
             $table->string('label');
             $table->string('link');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
