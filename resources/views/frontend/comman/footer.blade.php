<footer>
   <div class="footer-widgets-wrapper">
      <div class="container">
         <div class="column dt-sc-one-fourth first">
            <aside class="widget widget_text">
               <h3 class="widgettitle red_sketch"> About Kids Life </h3>
               <p>Happy <a href="#"><strong>Kids Life</strong></a> comes with powerful theme options, which empowers you to quickly and easily build incredible store.</p>
               <ul>
                  <li> <a href="#"> English Grammar Class </a> </li>
                  <li> <a href="#"> Music class </a> </li>
                  <li> <a href="#"> Swimming & Karate </a> </li>
                  <li> <a href="#"> Lot of HTML Styles </a> </li>
                  <li> <a href="#"> Unique News Page Design </a> </li>
               </ul>
            </aside>
         </div>
         <div class="column dt-sc-one-fourth">
            <aside class="widget widget_recent_entries">
               <h3 class="widgettitle green_sketch"> Latest Post </h3>
               <ul>
                  <li>
                     <a href="#">
                     <img src="{{url('assets/images/blog_list1.jpg')}}" alt="" title="">
                     </a>
                     <h6><a href="#"> Amazing post with all goodies </a></h6>
                     <span> March 23, 2014 </span>
                  </li>
                  <li>
                     <a href="#">
                     <img src="{{url('assets/images/blog_list2.jpg')}}" alt="" title="">
                     </a>
                     <h6><a href="#"> Amazing post with all goodies </a></h6>
                     <span> March 23, 2014 </span>
                  </li>
                  <li>
                     <a href="#">
                     <img src="{{url('assets/images/blog_list3.jpg')}}" alt="" title="">
                     </a>
                     <h6><a href="#"> Amazing post with all goodies </a></h6>
                     <span> March 23, 2014 </span>
                  </li>
               </ul>
            </aside>
         </div>
         <div class="column dt-sc-one-fourth">
            <aside class="widget tweetbox">
               <h3 class="widgettitle yellow_sketch"><a href="#"> Twitter Feeds </a></h3>
               <div id="tweets_container"></div>
            </aside>
         </div>
         <div class="column dt-sc-one-fourth">
            <aside class="widget widget_text">
               <h3 class="widgettitle steelblue_sketch">Contact</h3>
               <div class="textwidget">
                  <p class="dt-sc-contact-info"><span class="fa fa-map-marker"></span> 4318 Mansion House, Greenland <br> United States </p>
                  <p class="dt-sc-contact-info"><span class="fa fa-phone"></span> (000) 233 - 3236 </p>
                  <p class="dt-sc-contact-info"><span class="fa fa-envelope"></span><a href="https://wedesignthemes.com/cdn-cgi/l/email-protection#e59c8a90978b848880a5968a888088848c89cb868a88"> <span class="__cf_email__" data-cfemail="f3b89a97809f9a9596b3809c9e969e929a9fdd909c9e">[email&#160;protected]</span> </a></p>
               </div>
            </aside>
            <aside class="widget mailchimp">
               <p> We're social </p>
               <form name="frmnewsletter" class="mailchimp-form" action="https://wedesignthemes.com/html/kidslife/php/subscribe.php" method="post">
                  <p>
                     <span class="fa fa-envelope-o"> </span>
                     <input type="email" placeholder="Email Address" name="mc_email" required />
                  </p>
                  <input type="submit" value="Subscribe" class="button" name="btnsubscribe">
               </form>
               <div id="ajax_subscribe_msg"></div>
            </aside>
         </div>
      </div>
   </div>
   <div class="copyright">
      <div class="container">
         <p class="copyright-info">© 2014 Kids Life. All rights reserved. Design by <a href="#"  title=""></a></p>
         <div class="footer-links">
            <p>Follow us</p>
            <div class="main-footer">
               <div class="playstore">
                  <a href="https://apps.apple.com/in/app/neo-dales/id1437486384">
                  <img src="{{url('assets/images/app-store.png')}}" alt="" title=""></a>
               </div>
               <div class="google-play">
                  <a href="https://play.google.com/store/apps/details?id=com.kempsolutions.neodales">
                  <img src="{{url('assets/images/google-play.png')}}" alt="" title=""></a>
               </div>
            </div>
            <ul class="dt-sc-social-icons">
               <li class="facebook">
                  <a href="#">
                  <img src="{{url('assets/images/facebook.png')}}" alt="" title="">
                  </a>
               </li>
               <li class="twitter">
                  <a href="#">
                  <img src="{{url('assets/images/twitter.png')}}" alt="" title="">
                  </a>
               </li>
               <li class="gplus">
                  <a href="#">
                  <img src="{{('assets/images/gplus.png')}}" alt="" title="">
                  </a>
               </li>
               <li class="pinterest">
                  <a href="#">
                  <img src="{{url('assets/images/pinterest.png')}}" alt="" title=""></a>
               </li>
            </ul>
         </div>
      </div>
   </div>
</footer>
</div>
<a href="#" title="Go to Top" class="back-to-top">To Top ↑</a>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js' async ></script>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="assets/js/jquery.sticky.js"></script>
<script type="text/javascript" src="assets/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.inview.js"></script>
<script type="text/javascript" src="assets/js/validation.js"></script>
<script type="text/javascript" src="assets/js/jquery.tipTip.minified.js"></script>
<script type="text/javascript" src="assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="assets/js/twitter/jquery.tweet.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="assets/js/shortcodes.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/jquery-transit-modified.js"></script>
<script type="text/javascript" src="assets/js/layerslider.kreaturamedia.jquery.js"></script>
<script type='text/javascript' src="assets/js/greensock.js"></script>
<script type='text/javascript' src="assets/js/layerslider.transitions.js"></script>
<script type='text/javascript' src="assets/js/main.js"></script>
</body>
</html>