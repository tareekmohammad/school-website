<!DOCTYPE HTML>
<!--[if IE 7 ]>    
<html lang="en-gb" class="isie ie7 oldie no-js">
   <![endif]-->
   <!--[if IE 8 ]>    
   <html lang="en-gb" class="isie ie8 oldie no-js">
      <![endif]-->
      <!--[if IE 9 ]>    
      <html lang="en-gb" class="isie ie9 no-js">
         <![endif]-->
         <!--[if (gt IE 9)|!(IE)]><!-->
         <html lang="en-gb" class="no-js">
            <!--<![endif]-->
            <head>
               <meta http-equiv="content-Type" content="text/html; charset=utf-8">
               <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
               <title>Kids Life- Home</title>
               <meta name="description" content="">
               <meta name="author" content="">
               <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
               <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
               <link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css">
               <link href="{{url('assets/css/shortcodes.css')}}" rel="stylesheet" type="text/css">
               <link href="{{url('assets/css/responsive.css')}}" rel="stylesheet" type="text/css">
               <link href="{{url('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
               <link rel='stylesheet' id='layerslider-css' href="{{url('assets/css/layerslider.css')}}" type='text/css' media='all' />
               <link href="{{url('assets/css/prettyPhoto.css')}}" rel="stylesheet" type="text/css" media="all" />
               <!--[if IE 7]>
               <link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
               <![endif]-->
               <!--[if lt IE 9]>
               <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
               <![endif]-->
               <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel='stylesheet' type='text/css'>
               <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel='stylesheet' type='text/css'>
               <link href="https://fonts.googleapis.com/css?family=Bubblegum+Sans" rel='stylesheet' type='text/css'>
               <script src="{{url('assets/js/modernizr-2.6.2.min.js')}}"></script>
            </head>
            <body class="main">
               <div class="wrapper">
               <header>
                  <div class="container">
                     <div class="logo">
                        <a href="{{url('')}}" title="Kids Life"><img src="{{url('assets/images/logo.png')}}" alt="Kids Life" title="Kids Life"></a>
                     </div>
                     <div class="my-nava">
                        <!-- <ul class="dt-sc-social-icons">
                           <li><a href="#" title="Facebook" class="dt-sc-tooltip-top facebook"><span class="fa fa-facebook"></span></a></li>
                           <li><a href="#" title="Youtube" class="dt-sc-tooltip-top youtube"><span class="fa fa-youtube"></span></a></li>
                           <li><a href="#" title="Twitter" class="dt-sc-tooltip-top twitter"><span class="fa fa-twitter"></span></a></li>
                           <li><a href="#" title="Google Plus" class="dt-sc-tooltip-top gplus"><span class="fa fa-google-plus"></span></a></li>
                        </ul> -->
                        <div class="contact-details">
                        <p class="mail">
                        <a href="#"><span class="__cf_email__" data-cfemail="e3808c8d97828097a3888a87908f8a8586cd808c8e">[email&nbsp;protected]</span></a>
                        <span class="fa fa-envelope"></span>
                        </p>
                        <p class="phone-no">
                        <i>+1 959 552 5963</i>
                        <span class="fa fa-phone"></span>
                        </p>
                        <p>
                        <!-- <a href="#">
                        <i>Login</i>
                        <span class="fa fa-sign-in"></span>
                         </a> -->
                        </p>
                        </div>
                     </div>
                  </div>
                  <div id="menu-container">
                     <div class="container">
                        <nav id="main-menu">
                           <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
                           <ul id="menu-main-menu" class="menu">
                              <!-- <li class="current_page_item menu-item-simple-parent menu-item-depth-0 red">
                                 <a href="index.html">HOME</a>
                              </li> -->
                              <li class="mustard current_page_item menu-item-simple-parent menu-item-depth-0">
                                 <a href="#">ABOUT</a>
                                 <ul class="sub-menu">
                                    <li> <a href="{{url('ourvision')}}">Our Vision & Mission</a> </li>
                                    <li> <a href="#">Chairman's Message</a> </li>
                                    <li> <a href="#"> Principal's Message</a></li>
                                    <li> <a href="#"> Staff</a></li>
                                    <li> <a href="services.html"> Careers </a> </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                              <li class="green current_page_item menu-item-simple-parent menu-item-depth-0">
                                 <a href="#"> Admissions </a>
                                 <ul class="sub-menu">
                                    <li> <a href="{{url('register')}}"> Alumni Registration </a> </li>
                                    <li> <a href="#"> Admission Procedure </a> </li>
                                    <li> <a href="#"> Fee Structure </a> </li>
                                    <li> <a href="{{url('admissionenquiry')}}"> Make an Admission Enquiry </a> </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                              <li class="menu-item-simple-parent menu-item-depth-0 lavender">
                                 <a href="#" title="">Learning</a>
                                 <ul class="sub-menu">
                                    <li>
                                       <a href="#">Academics</a>
                                       <ul class="sub-menu">
                                          <li><a href="#">The Curriculum</a></li>
                                          <li><a href="#">Support for Learning</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                    <li>
                                       <a href="#">Sports</a>
                                       <ul class="sub-menu">
                                          <li><a href="#">Curricular Sports</a></li>
                                          <li><a href="#">Interschool Participation</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                    <li>
                                       <a href="#">Art & Culture</a>
                                       <ul class="sub-menu">
                                          <li><a href="#">Visual Arts</a></li>
                                          <li><a href="#">Performing Arts</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                    <li>
                                       <a href="#">Beyond the Classroom</a>
                                       <ul class="sub-menu">
                                          <li><a href="#">SPICMACAY</a></li>
                                          <li><a href="#">NCC</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                              <li class="menu-item-simple-parent menu-item-depth-0 pink">
                                 <a href="#" title="">Campus Life</a>
                                 <ul class="sub-menu">
                                    <li>
                                       <a href="#">Infrastructure</a>
                                       <ul class="sub-menu">
                                          <li><a href="#">Campus</a></li>
                                          <li><a href="#">Hostels</a></li>
                                          <li><a href="#">Sports Facilities</a></li>
                                          <li><a href="#">Other Facilities</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                    <li>
                                       <a href="#">Boarding Pupils</a>
                                       <ul class="sub-menu">
                                          <li><a href="#">Health & Wellbeing</a></li>
                                          <li><a href="#">Personal Development</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                    <li>
                                       <a href="#">Achievers</a>
                                       <ul class="sub-menu">
                                          <li><a href="#">Academic</a></li>
                                          <li><a href="#">Sports & Other Activities</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                              <li class="menu-item-simple-parent menu-item-depth-0 steelblue">
                                 <a href="#" title="">News & Events</a>
                                 <ul class="sub-menu">
                                    <li>
                                       <a href="{{url('news')}}">News</a>
                                    </li>
                                    <li>
                                       <a href="{{url('blog')}}">Blog</a>
                                    </li>
                                    <li>
                                       <a href="blog.html">Upcoming events</a>
                                    </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                              <li class="menu-item-simple-parent menu-item-depth-0 purple">
                                 <a href="#" title="">Gallery</a>
                                 <ul class="sub-menu">
                                    <li>
                                       <a href="{{url('picture')}}">Picture</a>
                                    </li>
                                    <li>
                                       <a href="{{url('video')}}">Video</a>
                                    </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                              <!-- <li class="current_page_item menu-item-simple-parent menu-item-depth-0 red">
                                 <a href="index.html"> Alumni </a>
                                 <ul class="sub-menu">
                                    <li> <a href="{{url('register')}}"> Alumni Registration </a> </li>
                                    <li> <a href="#"> Submenu Level I </a> </li>
                                    <li> <a href="#"> Submenu Level I </a> </li>
                                    <li> <a href="#"> Submenu Level I </a> </li>
                                    <li>
                                       <a href="#"> Submenu Level I </a>
                                       <ul class="sub-menu">
                                          <li> <a href="#"> Submenu Level II </a> </li>
                                          <li> <a href="#"> Submenu Level II </a> </li>
                                          <li> <a href="#"> Submenu Level II </a> </li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                                 </li> -->
                              <li class="mustard"> <a href="about.html"> Guests & Visitors </a> </li>
                              <li class="menu-item-simple-parent menu-item-depth-0 pink">
                                 <a href="#" title="">Download</a>
                                 <ul class="sub-menu">
                                    <li>
                                       <a href="{{url('kitlist')}}">Kit List</a>
                                    </li>
                                    <li>
                                       <a href="#">Tc</a>
                                    </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                              <li class="menu-item-simple-parent menu-item-depth-0 lavender">
                                 <a href="{{ url('contact')}}" title="">Contact</a>
                                 <ul class="sub-menu">
                                    <li>
                                       <a href="blog-two-column.html">Our Location</a>
                                       <ul class="sub-menu">
                                          <li><a href="blog-two-column-with-sidebar.html">With Sidebar</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                    <li>
                                       <a href="blog.html">Blog One Column</a>
                                       <ul class="sub-menu">
                                          <li><a href="blog-with-sidebar.html">With Sidebar</a></li>
                                       </ul>
                                       <a class="dt-menu-expand">+</a>
                                    </li>
                                 </ul>
                                 <a class="dt-menu-expand">+</a>
                              </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </header>