@extends('frontend.layout.master')
@section('contant')
<div id="main">
   <div class="breadcrumb-section">
      <div class="container">
         <h1>KIT LIst</h1>
         <div class="breadcrumb">
            <a href="index.html">Home</a>
            <span class="fa fa-angle-double-right"></span>
            <span class="current">Download Pdf</span>
         </div>
      </div>
   </div>
   <div class="container">
      <section id="primary" class="content-full-width mainpicture">
         <div class="row">
      	   <div class="col-lg-4">
               <div class="portfolio-thumb">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="portfolio-thumb">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="portfolio-thumb">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="portfolio-thumb">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                  </div>
               </div>
            </div>

         </div>
            

           

            
      </section>
      <div class="my-button">
      <a href="#" class="dt-sc-button medium"> Download <span class="fa fa-chevron-circle-right"> </span></a>
      <div class="dt-sc-clear"></div>
      </div>
   </div>

</div>
@endsection
