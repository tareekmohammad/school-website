@extends('frontend.layout.master')
@section('contant')
<div id="main">
   <div id="slider">
      <div id="layerslider_4" class="ls-wp-container" style="width:100%;height:510px;max-width:1920px;">
         <div class="ls-slide" data-ls="slidedelay:7000; transition2d: all;">
            <img src="{{url('assets/images/IMG11.jpg')}}" class="ls-bg" style="background-size:cover;width: 100%;" alt="Slide background" />
            
         </div>
         <div class="ls-slide" data-ls="slidedelay:7000; transition2d: all;">
            <img src="{{url('assets/images/IMG11.jpg')}}" class="ls-bg" style="background-size:cover;width: 100%;" alt="Slide background" />
            
         </div>
         <div class="ls-slide" data-ls="slidedelay:7000; transition2d: all;">
            <img src="assets/images/layerslider-gallery/black-board.jpg" style="background-size:cover;width: 100%;" class="ls-bg" alt="Slide background" />
            jrtgh
         </div>
         </div>
      </div>
   </div>
   <section class="fullwidth-background main-news dt-sc-parallax-section turquoise-bg" style="background-position: 50% 54px;">
      <div class="news-section-about-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-12">
                  <div class="peraabout">
                  <h2>About</h2>
                  <p> is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                 </div>
               </div>
               <div class="col-lg-6 col-12">
                  <div class="news-header">
                     <h2>News</h2>
                  </div>
                  <div class="news-mainsection">
                     <marquee class="news-section" scrollamount="3" onmouseover="stop()" height="200" onmouseout="start()" behavior="scroll" direction="up">
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <br>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                        <a href="news/1">ution of letters, as opposed to using 'Content her</a>
                     </marquee>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="primary" class="content-full-width">
      <div class="container">
         <div class="dt-sc-one-fourth column first">
            <div class="dt-sc-ico-content type1">
               <div class="icon">
                  <span class="icon-outer">
                  <img src="{{url('assets/images/service1.jpg')}}" alt="" title="">
                  <span class="infolayer">
                  <a href="#"><i class="fa fa-link"></i></a>
                  </span>
                  </span>
               </div>
               <h4><a href="#">Active Learning</a></h4>
               <p>Curabitur ultrices posuere mattis. Nam ullamcorper, diam sit amet euismod pelleo ntesque, eros risus rhoncus libero, invest tibulum nisl ligula</p>
            </div>
         </div>
         <div class="dt-sc-one-fourth column">
            <div class="dt-sc-ico-content type1">
               <div class="icon">
                  <span class="icon-outer">
                  <img src="{{url('assets/images/service2.jpg')}}" alt="" title="">
                  <span class="infolayer">
                  <a href="#"><i class="fa fa-link"></i></a>
                  </span>
                  </span>
               </div>
               <h4><a href="#">Music Class</a></h4>
               <p>Decor ostdcaer urabitur ultrices posuere mattis. Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, invest tibulum nisl ligula</p>
            </div>
         </div>
         <div class="dt-sc-one-fourth column">
            <div class="dt-sc-ico-content type1">
               <div class="icon">
                  <span class="icon-outer">
                  <img src="{{url('assets/images/service3.jpg')}}" alt="" title="">
                  <span class="infolayer">
                  <a href="#"><i class="fa fa-link"></i></a>
                  </span>
                  </span>
               </div>
               <h4><a href="#">Yoga Class</a></h4>
               <p>Rabitur ultrices posuere mattis. Nam ullamcorper, diam sit euismod pelleo ntesque, eros risus rhoncus libero, invest tibulum nisl gedretu osterftra ligula</p>
            </div>
         </div>
         <div class="dt-sc-one-fourth column">
            <div class="dt-sc-ico-content type1">
               <div class="icon">
                  <span class="icon-outer">
                  <img src="{{url('assets/images/service4.jpg')}}" alt="" title="">
                  <span class="infolayer">
                  <a href="#"><i class="fa fa-link"></i></a>
                  </span>
                  </span>
               </div>
               <h4><a href="#">Kung fu Class</a></h4>
               <p>Curabitur ultrices posuere mattis. Nam ullamcorper, diam sit amet euismod pelleo ntesque, eros risus rhoncus libero, invest tibulum nisl ligula</p>
            </div>
         </div>
      </div>
      <div class="dt-sc-hr"></div>
      <section class="fullwidth-background dt-sc-parallax-section turquoise-bg">
         <div class="container">
            <h2>Play As You Learn</h2>
            <div class="row">
               <div class="col-lg-6 col-12">
                  <div class="dt-sc-ico-content type2">
                     <div class="icon">
                        <span class="fa fa-glass"> </span>
                     </div>
                     <h4><a href="#" target="_blank"> English Summer Camp </a></h4>
                     <p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
                  </div>
                  <div class="dt-sc-hr-very-small"></div>
                  <div class="dt-sc-ico-content type2">
                     <div class="icon">
                        <span class="fa fa-pencil"> </span>
                     </div>
                     <h4><a href="#" target="_blank"> Drawing & Painting </a></h4>
                     <p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
                  </div>
                  <div class="dt-sc-hr-very-small"></div>
                  <div class="dt-sc-ico-content type2">
                     <div class="icon">
                        <span class="fa fa-bullseye"> </span>
                     </div>
                     <h4><a href="#" target="_blank"> Swimming Camp </a></h4>
                     <p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
                  </div>
               
                  <div class="dt-sc-ico-content type2">
                     <div class="icon">
                        <span class="fa fa-tachometer"> </span>
                     </div>
                     <h4><a href="#" target="_blank"> Sports Camp </a></h4>
                     <p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
                  </div>
                  <div class="dt-sc-hr-very-small"></div>
                  <div class="dt-sc-ico-content type2">
                     <div class="icon">
                        <span class="fa fa-magic"> </span>
                     </div>
                     <h4><a href="#" target="_blank"> Personalizing </a></h4>
                     <p>Nam ullamcorper, diam sit amet euismod pelleontesque, eros risus rhoncus libero, inst tibulum nisl ligula....</p>
                  </div>
               </div>
            <div class="col-lg-6 col-12">
               <h2>With Music4Kids, music is child's play!</h2>
               <div class="add-slider-wrapper">
                  <ul class="add-slider">
                     <li> <img src="{{url('assets/images/add1.jpg')}}" alt="" title=""> </li>
                     <li> <img src="{{url('assets/images/add2.jpg')}}" alt="" title=""> </li>
                     <li> <img src="{{url('assets/images/add1.jpg')}}" alt="" title=""> </li>
                  </ul>
               </div>
            </div>
             </div>
         </div>
      </section>
      <div class="dt-sc-hr"></div>
      <div class="container">
         <h2 class="dt-sc-hr-green-title">Our Portfolio</h2>
         <div class="front-portfolio-container">
            <div class="portfolio-content portfolio-content1">
               <div class="front-portfolio">
                  <div class="portfolio-outer">
                     <div class="portfolio-thumb">
                        <img src="{{url('assets/images/image1.jpg')}}" alt="" title="">
                        <div class="image-overlay">
                           <h5><a href="portfolio-detail.html">Gifts at Large</a></h5>
                           <!-- <a href="portfolio-detail.html" class="link"><span class="fa fa-link"></span></a> -->
                           <a href="{{url('assets/images/image1.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="portfolio-content portfolio-content2">
               <div class="front-portfolio">
                  <div class="portfolio-outer">
                     <div class="portfolio-thumb">
                        <img src="{{url('assets/images/image2.jpg')}}" alt="" title="">
                        <div class="image-overlay">
                           <h5><a href="portfolio-detail.html">Gifts at Large</a></h5>
                           <!-- <a href="portfolio-detail.html" class="link"><span class="fa fa-link"></span></a> -->
                           <a href="{{url('assets/images/image2.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="portfolio-content portfolio-content3">
               <div class="front-portfolio">
                  <div class="portfolio-outer">
                     <div class="portfolio-thumb">
                        <img src="{{url('assets/images/image3.jpg')}}" alt="" title="">
                        <div class="image-overlay">
                           <h5><a href="portfolio-detail.html">Gifts at Large</a></h5>
                           <!-- <a href="portfolio-detail.html" class="link"><span class="fa fa-link"></span></a> -->
                           <a href="{{url('assets/images/image3.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="portfolio-content portfolio-content4">
               <div class="front-portfolio">
                  <div class="portfolio-outer">
                     <div class="portfolio-thumb">
                        <img src="{{url('assets/images/image4.jpg')}}" alt="" title="">
                        <div class="image-overlay">
                           <h5><a href="portfolio-detail.html">Gifts at Large</a></h5>
                           <!-- <a href="portfolio-detail.html" class="link"><span class="fa fa-link"></span></a> -->
                           <a href="{{url('assets/images/image4.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="portfolio-content portfolio-content5">
               <div class="front-portfolio">
                  <div class="portfolio-outer">
                     <div class="portfolio-thumb">
                        <img src="{{url('assets/images/image5.jpg')}}" alt="" title="">
                        <div class="image-overlay">
                           <h5><a href="portfolio-detail.html">Gifts at Large</a></h5>
                           <!-- <a href="portfolio-detail.html" class="link"><span class="fa fa-link"></span></a> -->
                           <a href="{{url('assetsimages/image5.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="portfolio-content portfolio-content6">
               <div class="front-portfolio">
                  <div class="portfolio-outer">
                     <div class="portfolio-thumb">
                        <img src="{{url('assets/images/image6.jpg')}}" alt="" title="">
                        <div class="image-overlay">
                           <h5><a href="portfolio-detail.html">Gifts at Large</a></h5>
                           <!-- <a href="portfolio-detail.html" class="link"><span class="fa fa-link"></span></a> -->
                           <a href="{{url('assets/images/image6.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="portfolio-content portfolio-content7">
               <div class="front-portfolio">
                  <div class="portfolio-outer">
                     <div class="portfolio-thumb">
                        <img src="{{url('assets/images/image7.jpg')}}" alt="" title="">
                        <div class="image-overlay">
                           <h5><a href="portfolio-detail.html">Gifts at Large</a></h5>
                           <!-- <a href="portfolio-detail.html" class="link"><span class="fa fa-link"></span></a> -->
                           <a href="{{url('assets/images/image7.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="dt-sc-hr-small"></div>
            <div class="aligncenter">
               <a href="#" class="dt-sc-button medium mustard"> Hit here to view all <span class="fa fa-chevron-circle-right"> </span></a>
            </div>
         </div>
      </div>
      <div class="dt-sc-hr"></div>
      <section class="fullwidth-background dt-sc-parallax-section orange-bg">
         <div class="container">
            <h2 class="dt-sc-hr-white-title">Recent Blog</h2>
            <div class="column dt-sc-one-half first">
               <article class="blog-entry">
                  <div class="blog-entry-inner">
                     <div class="entry-meta">
                        <a href="blog-detail.html" class="blog-author"><img src="{{url('assets/images/rounded-bg-img.jpg')}}" alt="" title=""></a>
                        <div class="date">
                           <span> 27 </span>
                           <p> Aug <br> 2014 </p>
                        </div>
                        <a href="#" class="comments">
                        12 <span class="fa fa-comment"> </span>
                        </a>
                        <a href="#" class="entry_format"><span class="fa fa-picture-o"></span></a>
                     </div>
                     <div class="entry-thumb">
                        <a href="blog-detail.html"><img src="{{url('assets/images/blog_img1.jpg')}}" alt="" title=""></a>
                     </div>
                     <div class="entry-details">
                        <div class="entry-title">
                           <h3><a href="blog-detail.html"> Activities Improves Mind </a></h3>
                        </div>
                        <div class="entry-body">
                           <p>Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums.</p>
                        </div>
                        <a href="blog-detail.html" class="dt-sc-button small"> Read More <span class="fa fa-chevron-circle-right"> </span></a>
                     </div>
                  </div>
               </article>
            </div>
            <div class="column dt-sc-one-half">
               <article class="blog-entry">
                  <div class="blog-entry-inner">
                     <div class="entry-meta">
                        <a href="blog-detail.html" class="blog-author"><img src="{{url('assets/images/rounded-bg-img.jpg')}}" alt="" title=""></a>
                        <div class="date">
                           <span> 27 </span>
                           <p> Aug <br> 2014 </p>
                        </div>
                        <a href="#" class="comments">
                        12 <span class="fa fa-comment"> </span>
                        </a>
                        <a href="#" class="entry_format"><span class="fa fa-picture-o"></span></a>
                     </div>
                     <div class="entry-thumb">
                        <a href="blog-detail.html"><img src="{{url('assets/images/blog_img2.jpg')}}" alt="" title=""></a>
                     </div>
                     <div class="entry-details">
                        <div class="entry-title">
                           <h3><a href="blog-detail.html"> Weekly Reader Zone </a></h3>
                        </div>
                        <div class="entry-body">
                           <p>Iid venenatis nibh accums. Doinbibe ndum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accumsan...</p>
                        </div>
                        <a href="blog-detail.html" class="dt-sc-button small"> Read More <span class="fa fa-chevron-circle-right"> </span></a>
                     </div>
                  </div>
               </article>
            </div>
         </div>
      </section>
      <div class="dt-sc-hr"></div>
      <div class="container">
         <h2 class="dt-sc-hr-green-title">Our Staffs</h2>
         <div class="column dt-sc-one-fourth first">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="{{url('assets/images/mask.png')}}" alt="" title="">
                  <img src="{{url('assets/images/team1.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Jack Daniels </h4>
                  <h6> Senior Supervisor </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
         <div class="column dt-sc-one-fourth">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="{{url('assets/images/mask.png')}}" alt="" title="">
                  <img src="{{url('assets/images/team2.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Linda Glendell </h4>
                  <h6> Teaching Professor </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
         <div class="column dt-sc-one-fourth">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="assets/images/mask.png" alt="" title="">
                  <img src="{{url('assets/images/team3.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Kate Dennings </h4>
                  <h6> Children Diet </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
         <div class="column dt-sc-one-fourth">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="{{url('assets/images/mask.png')}}" alt="" title="">
                  <img src="{{url('assets/images/team4.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Kristof Slinghot </h4>
                  <h6> Management </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
      </div>
      <div class="dt-sc-hr"></div>
      <section class="fullwidth-background dt-sc-parallax-section product_bg">
         <div class="container">
            <h2 class="dt-sc-hr-white-title">Happy Parent's Say</h2>
            <div class="about-slider-wrapper main-adslider">
               <div class="add-slider">
                  <div class="testimonial-main">
                     <p class="font-15 text-white font-weight-600">It is wonderful to be able to know exactly what my child do, learn, and play. TMS is a dream place for my child, where she enjoy and learn in a fun- to- do way. It is awesome platform for the mutual benefit of child, parents and teachers, helping us making a strong interpersonal bond for the future endeavour.</p>
                     <div class="hedding-bottom">
                        <h4>Lt Col Amar Madev Bhosale</h4>
                        <div class="testimonial-img">
                           <img src="{{url('assets/images/team2.jpg')}}">
                        </div>
                     </div>
                  </div>
                  <div class="testimonial-main">
                     <p class="font-15 text-white font-weight-600">It is wonderful to be able to know exactly what my child do, learn, and play. TMS is a dream place for my child, where she enjoy and learn in a fun- to- do way. It is awesome platform for the mutual benefit of child, parents and teachers, helping us making a strong interpersonal bond for the future endeavour.</p>
                     <div class="hedding-bottom">
                        <h4>Lt Col Amar Madev Bhosale</h4>
                        <div class="testimonial-img">
                           <img src="{{url('assets/images/team2.jpg')}}">
                        </div>
                     </div>
                  </div>
                  <div class="testimonial-main">
                     <p class="font-15 text-white font-weight-600">It is wonderful to be able to know exactly what my child do, learn, and play. TMS is a dream place for my child, where she enjoy and learn in a fun- to- do way. It is awesome platform for the mutual benefit of child, parents and teachers, helping us making a strong interpersonal bond for the future endeavour.</p>
                     <div class="hedding-bottom">
                        <h4>Lt Col Amar Madev Bhosale</h4>
                        <div class="testimonial-img">
                           <img src="{{url('assets/images/team2.jpg')}}">
                        </div>
                     </div>
                  </div>
                  <div class="testimonial-main">
                     <p class="font-15 text-white font-weight-600">It is wonderful to be able to know exactly what my child do, learn, and play. TMS is a dream place for my child, where she enjoy and learn in a fun- to- do way. It is awesome platform for the mutual benefit of child, parents and teachers, helping us making a strong interpersonal bond for the future endeavour.</p>
                     <div class="hedding-bottom">
                        <h4>Lt Col Amar Madev Bhosale</h4>
                        <div class="testimonial-img">
                       <img src="{{url('assets/images/team2.jpg')}}">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </section>
</div>
@endsection