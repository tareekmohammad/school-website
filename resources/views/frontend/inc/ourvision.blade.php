@extends('frontend.layout.master')
@section('contant')
<div id="main">
   <section class="fullwidth_banner">
      <div class="container">
         <h1>Our Vision & Mission</h1>
      </div>
   </section>
   <section class="ourvisition">
   <div class="container">
      <div class="row">
         <div class="col-lg-6">
            <div class="about-slider-wrapper">
               <ul class="about-slider">
                  <li> <img src="{{url('assets/images/activity1.jpg')}}" alt="" title=""> </li>
                  <li> <img src="{{url('assets/images/activity2.jpg')}}" alt="" title=""> </li>
                  <li> <img src="{{url('assets/images/activity3.jpg')}}" alt="" title=""> </li>
               </ul>
            </div>
         </div>
         <div class="col-lg-6">
            <h2>At NPS, we provide opportunities for every child to excel.</h2>
            <p>Our mission is to provide conducive environment for academic, social and extra-curricular, cultural, physical and emotional growth. Its supportive in all possible ways and gives opportunities for all pupils to explore, experience, learn to excel in life.
               Our vision is to support, motivate and inspire young and budding minds to shine during their time at school and beyond, by nurturing the most of their individual abilities, unique potential, talent and raising them to excellence through their holistic development. Education with values and respect for culture & heritage is our utmost concern and we endeavor to develop every child into a responsible world citizen on which Parents, Society and Nation will be proud of.
               We celebrate individuality, yet work together as a strong community of pupils, parents, staff and alumni.
               Regardless of who we are and where our strengths lie, we all have the ability to achieve excellence and will be encouraged and supported to do so.
               We conduct ourselves with honesty and trustworthiness and take pride in our work and relationships. 
            </p>
           <!--  <a href="#" class="dt-sc-button small read-more"> Read More <span class="fa fa-chevron-circle-right"> </span></a> -->
         </div>
         <div class="dt-sc-hr"></div>
         <h2 class="dt-sc-hr-green-title">Our Teachers</h2>
         <div class="column dt-sc-one-fourth first">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="images/mask.png" alt="" title="">
                  <img src="{{url('assets/images/team1.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Jack Daniels </h4>
                  <h6> Senior Supervisor </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
         <div class="column dt-sc-one-fourth">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="images/mask.png" alt="" title="">
                  <img src="{{url('assets/images/team2.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Linda Glendell </h4>
                  <h6> Teaching Professor </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
         <div class="column dt-sc-one-fourth">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="images/mask.png" alt="" title="">
                  <img src="{{url('assets/images/team3.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Kate Dennings </h4>
                  <h6> Children Diet </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
         <div class="column dt-sc-one-fourth">
            <div class="dt-sc-team">
               <div class="image">
                  <img class="item-mask" src="images/mask.png" alt="" title="">
                  <img src="{{url('assets/images/team4.jpg')}}" alt="" title="">
                  <div class="dt-sc-image-overlay">
                     <a href="#" class="link"><span class="fa fa-facebook"></span></a>
                     <a href="#" class="zoom"><span class="fa fa-twitter"></span></a>
                  </div>
               </div>
               <div class="team-details">
                  <h4> Kristof Slinghot </h4>
                  <h6> Management </h6>
                  <p> Phasellus lorem augue, vulputate vel orci id, ultricies aliquet risus. </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
</div>

@endsection