@extends('frontend.layout.master')
@section('contant')
<div id="main">
   <div class="breadcrumb-section">
      <div class="container">
         <h1>School Picture</h1>
         <div class="breadcrumb">
            <a href="index.html">Home</a>
            <span class="fa fa-angle-double-right"></span>
            <span class="current">School Picture</span>
         </div>
      </div>
   </div>
   <div class="container">
   <section id="primary" class="content-full-width mainpicture">
      <div class="container">
         <div class="row">
            <div class="col-lg-4">
               <div class="portfolio">
                  <div class="portfolio-thumb">
                     <div class="activity my-activiti box1">
                        <h4> Creative Writing </h4>
                        <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     </div>
                     <div class="image-overlay">
                        <a href="{{url('assets/images/activity1.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="portfolio">
                  <div class="portfolio-thumb">
                     <div class="activity my-activiti box1">
                        <h4> Creative Writing </h4>
                        <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     </div>
                     <div class="image-overlay">
                        <a href="{{url('assets/images/activity1.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="portfolio">
                  <div class="portfolio-thumb">
                     <div class="activity my-activiti box1">
                        <h4> Creative Writing </h4>
                        <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     </div>
                     <div class="image-overlay">
                        <a href="{{url('assets/images/activity1.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="portfolio">
                  <div class="portfolio-thumb">
                     <div class="activity my-activiti box1">
                        <h4> Creative Writing </h4>
                        <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     </div>
                     <div class="image-overlay">
                        <a href="{{url('assets/images/activity1.jpg')}}" data-gal="prettyPhoto[gallery]" class="zoom"><span class="fa fa-search"></span></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
</div>
@endsection
