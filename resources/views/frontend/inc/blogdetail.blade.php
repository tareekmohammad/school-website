@extends('frontend.layout.master')
@section('contant')
<div id="main">

<div class="breadcrumb-section">
<div class="container">
<h1>Activities Improves Mind</h1>
<div class="breadcrumb">
<a href="index.html">Home</a>
<span class="fa fa-angle-double-right"></span>
<a href="blog.html">Blog</a>
<span class="fa fa-angle-double-right"></span>
<span class="current">Activities Improves Mind</span>
</div>
</div>
</div>


<div class="container">

<section id="primary" class="with-sidebar">
<article class="blog-entry">
<div class="blog-entry-inner">
<div class="entry-meta">
<a href="#" class="blog-author"><img src="images/rounded-bg-img.jpg" alt="" title=""></a>
<div class="date">
<span> 27 </span>
<p> Aug <br> 2014 </p>
</div>
<a href="#" class="comments">
12 <span class="fa fa-comment"> </span>
</a>
<a href="#" class="entry_format"><span class="fa fa-picture-o"></span></a>
</div>
<div class="entry-thumb">
<a href="#"><img src="images/blog_img1.jpg" alt="" title=""></a>
</div>
<div class="entry-details">
<div class="entry-title">
 <h3> Activities Improves Mind </h3>
</div>

<div class="entry-body">
<p>Roin bibendum nibhsds. Nuncsdsd fermdada msit ametadasd consequat. Praes porr nulla sit amet dui lobortis, id venenatis nibh accums. Proin lobortis tempus odio eget venenatis. Proin fermentum ut massa at bibendum. Proin bibendum non est quis egestas. Pellentesque at enim id enim tempus placerat. Etiam tempus gravida leo, et gravida justo bibendum non. Suspendisse vitae fermentum sapien.</p>
<blockquote class="alignright">
<q>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Maecenas nec odio et ante tincidunt tempus.</q>
</blockquote>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sollicitudin interdum eros at pellentesque. Donec dictum rhoncus sapien aliquet tempor. Cras cursus nisi sed dui scelerisque posuere. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Maecenas nec odio et ante tincidunt tempus.</p>
<p>Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Maecenas nec odio et ante tincidunt tempus. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Maecenas nec odio et ante tincidunt tempus.</p>
</div>
</div>
</div>
</article>

<div class="commententries">
<h2 class="dt-sc-title"><span>Comments</span></h2>
<ul class="commentlist">
<li>
<article class="comment">
<header class="comment-author">
<img class="item-mask" src="images/author-hexa-bg.png" alt="" title="">
<img src="images/comment1.jpg" alt="" title="">
</header>
<section class="comment-details">
<div class="author-name">
<a href="#">Jeniffer Conolley</a>
</div>
<div class="commentmetadata">21 Nov 2012</div>
<div class="comment-body">
<div class="comment-content">
<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure.</p>
</div>
</div>
<div class="reply">
<a class="comment-reply-link" href="#">Reply <span class="fa fa-angle-double-right"></span></a>
</div>
</section>
</article>
<ul class="children">
<li>
<article class="comment">
<header class="comment-author">
<img class="item-mask" src="images/author-hexa-bg.png" alt="" title="">
<img src="images/comment2.jpg" alt="" title="">
</header>
<section class="comment-details">
<div class="author-name">
<a href="#">Lilly Dafoe</a>
</div>
<div class="commentmetadata">21 Nov 2012</div>
<div class="comment-body">
<div class="comment-content">
<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure.</p>
</div>
</div>
<div class="reply">
<a class="comment-reply-link" href="#">Reply <span class="fa fa-angle-double-right"></span></a>
</div>
</section>
</article>
</li>
</ul>
</li>
<li>
<article class="comment">
<header class="comment-author">
<img class="item-mask" src="images/author-hexa-bg.png" alt="" title="">
<img src="images/comment3.jpg" alt="" title="">
</header>
<section class="comment-details">
<div class="author-name">
<a href="#">Michael Richards</a>
</div>
<div class="commentmetadata">21 Nov 2012</div>
<div class="comment-body">
<div class="comment-content">
<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure.</p>
</div>
</div>
<div class="reply">
<a class="comment-reply-link" href="#">Reply <span class="fa fa-angle-double-right"></span></a>
</div>
</section>
</article>
</li>
</ul>
<div id="respond" class="comment-respond">
<h2>Enroll Your Words</h2>
<form method="post" action="#" id="commentform" class="comment-form">
<p class="column dt-sc-one-half first">
<input id="author" name="author" type="text" placeholder="Name" required="">
</p>
<p class="column dt-sc-one-half">
<input id="email" name="email" type="email" placeholder="Email ID" required="">
</p>
<p>
<textarea id="comment" name="comment" placeholder="Message"></textarea>
</p>
<p>
<input name="submit" type="submit" id="submit" value="Add Comment">
</p>
</form>
</div>
</div>

</section>


<section id="secondary">
<aside class="widget widget_categories">
<h3 class="widgettitle">Categories</h3>
<ul>
<li>
<a href="#">Play School<span>(16)</span></a>
</li>
<li>
<a href="#">Academic Performance<span>(3)</span></a>
</li>
<li>
<a href="#">Co-curricular<span>(26)</span></a>
</li>
<li>
<a href="#">Visual Education<span>(18)</span></a>
</li>
<li>
<a href="#">Inter Competition<span>(4)</span></a>
</li>
</ul>
</aside>
<aside class="widget widget_text">
<h3 class="widgettitle">Kids Achievements</h3>
<p>In lobortis rhoncus pulvinar. Pellentesque habitant morbi tristique <a href="#" class="highlighter">senectus</a> et netus et malesuada fames ac turpis egestas. </p>
<p>Sed tempus ligula ac mi iaculis lobortis. Nam consectetur justo non nisi dapibus, ac commodo mi sagittis. Integer enim odio.</p>
</aside>
<aside class="widget widget_text">
<h3 class="widgettitle">Visual Guidance</h3>
<p>Our methods of teaching and level of quality instructors all add up to a well-rounded experience.</p>
<iframe src="https://player.vimeo.com/video/21195297" width="420" height="200"></iframe>
</aside>
<aside class="widget widget_recent_entries">
<h3 class="widgettitle">Kids Voices</h3>

<div class="dt-sc-tabs-container">
<ul class="dt-sc-tabs">
<li><a href="#"> New </a></li>
<li><a href="#"> Popular </a></li>
</ul>
<div class="dt-sc-tabs-content">
<h5><a href="#">Explore your Thoughts!</a></h5>
<p>Nam consectetur justo non nis dapibus, ac commodo mi sagittis. Integer enim odio.</p>
<h5><a href="#">Perform for Success!</a></h5>
<p>Sed ut perspiciatis unde omi iste natus error siterrecte voluptatem accusantium doloremque laudantium.</p>
</div>
<div class="dt-sc-tabs-content">
<h5><a href="#">Admire &amp; Achieve!</a></h5>
<p>Sed ut perspiciatis unde omi iste natus error siterrecte voluptatem accusantium doloremque laudantium.</p>
<h5><a href="#">Your Opportuntiy!</a></h5>
<p>Nam consectetur justo non nis dapibus, ac commodo mi sagittis. Integer enim odio.</p>
</div>
</div>

</aside>
<aside class="widget widget_tag_cloud">
<h3 class="widgettitle">Hit on Tags</h3>
<div class="tagcloud">
<a href="#">Listen</a>
<a href="#">Observe</a>
<a href="#">Admire</a>
<a href="#">Accomplish</a>
<a href="#">Perform</a>
<a href="#">Achieve</a>
<a href="#">Target</a>
</div>
</aside>
</section>

</div>

</div>
@endsection