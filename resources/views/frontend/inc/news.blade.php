@extends('frontend.layout.master')
@section('contant')
<div id="main">
   <section class="fullwidth_banner">
      <div class="container">
         <h1>News</h1>
      </div>
   </section>
   <div class="container">
      <section id="primary" class="content-full-width">
         <h2 class="dt-sc-hr-green-title"> Activities for Children </h2>
         <div class="row">
            <div class="col-lg-4">
               <div class="mar-bottommy">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     <p>Vestibulum Ante Ipsum Primis In Faucibus Orci Luctus Et Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="mar-bottommy">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     <p>Vestibulum Ante Ipsum Primis In Faucibus Orci Luctus Et Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="mar-bottommy">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     <p>Vestibulum Ante Ipsum Primis In Faucibus Orci Luctus Et Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="mar-bottommy">
                  <div class="activity box1">
                     <h4> Creative Writing </h4>
                     <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                     <p>Vestibulum Ante Ipsum Primis In Faucibus Orci Luctus Et Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
</div>
@endsection