@extends('frontend.layout.master')
@section('contant')
<div id="main">
   <section class="fullwidth_banner">
      <div class="container">
         <h1>Alumni Registration</h1>
      </div>
   </section>
   <section id="primary" class="content-full-width">
      <div class="container">
         <div class="main-form-al">
            <div class="column dt-sc-one-half first">
               <div class="form-register">
                  <div class="from-my">
                     <label>
                        <h4>Year of Passing</h4>
                     </label>
                     <select name="syear" class="form-control" id="syear" required="">
                        <option value="">Year of Passing</option>
                        <option value="2001">2001</option>
                        <option value="2002">2002</option>
                        <option value="2003">2003</option>
                        <option value="2004">2004</option>
                        <option value="2005">2005</option>
                        <option value="2006">2006</option>
                        <option value="2007">2007</option>
                        <option value="2008">2008</option>
                        <option value="2009">2009</option>
                        <option value="2010">2010</option>
                        <option value="2011">2011</option>
                        <option value="2012">2012</option>
                        <option value="2013">2013</option>
                        <option value="2014">2014</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                     </select>
                  </div>
                  <div class="from-my">
                     <label>
                        <h4>Student's Name</h4>
                     </label>
                     <input type="text" class="form-control" placeholder="Student's Name" name="">
                  </div>
                  <div class="from-my">
                     <label>
                        <h4>Father's Name</h4>
                     </label>
                     <input type="text" class="form-control" placeholder="Father's Name" name="">
                  </div>
                  <div class="from-my">
                     <label>
                        <h4>Date of Birth (In Figures)</h4>
                     </label>
                     <input type="date" class="form-control" name="" placeholder="Date of Birth">
                  </div>
                  <div class="from-my">
                     <label>
                        <h4>Gender</h4>
                     </label>
                     <br>
                     <div class="main-gender">
                        <input type="radio" name="gender" id="yes" value="male" checked>
                        <label for="male">Male</label>
                        <br>
                        <input type="radio" name="gender" id="yes" value="female">
                        <label for="female">Female</label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="column dt-sc-one-half">
               <div class="form-register">
                  <div class="from-my">
                     <label>
                        <h4>City</h4>
                     </label>
                     <input type="text" class="form-control" name="" placeholder="City">
                  </div>
                  <div class="from-my">
                     <label>
                        <h4>State</h4>
                     </label>
                     <input type="text" class="form-control" name="" placeholder="State">
                  </div>
                  <div class="from-my">
                     <label>
                        <h4>Mobile No</h4>
                     </label>
                     <input type="text" class="form-control" name="" placeholder="Mobile No">
                  </div>
                  <div class="from-my">
                     <label>
                        <h4>E-mail Address</h4>
                     </label>
                     <input type="text" class="form-control" name="" placeholder="E-mail Address">
                  </div>
               </div>
            </div>
            <div class="float-clear"></div>
            <div class="main-address">
               <div class="from-my">
                  <label>
                     <h4>Present Designation, Organisation & Location</h4>
                  </label>
                  <input type="text" class="form-control" name="" placeholder="Present Designation, Organisation & Location">
               </div>
            </div>
            <div class="h4-main-bottom">
               <h4>Please recheck mobile number before submitting the form as you will receive a message carrying registration number on the same.</h4>
            </div>
            <div class="main-address">
               <a href="#" class="dt-sc-button small pink"> Submit <span class="fa fa-paper-plane"> </span></a>
            </div>
         </div>
      </div>
   </section>
</div>
@endsection