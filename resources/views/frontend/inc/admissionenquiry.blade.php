@extends('frontend.layout.master')
@section('contant')
<?php
 
  if(!empty($_POST['g-recaptcha-response']))
  {
        $secret = 'GOOGLE_CAPTACH_SECRET_KEY';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success)
            $message = "g-recaptcha varified successfully";
        else
            $message = "Some error in vrifying g-recaptcha";
       echo $message;
   }
?><div id="main">

<section class="fullwidth_banner">
<div class="container">
<h1>Make an Admission Enquiry</h1>
</div>
</section>
<section id="primary" class="content-full-width">
  <div class="container">
  	<div class="main-form-al">
  	<div class="column dt-sc-one-half first">
        <form id="frmContact" action="varify_captcha.php" method="POST" novalidate="novalidate">
   
   
	 <div class="form-register">
	  	<div class="from-my">
	  	<label><h4>Year of Passing</h4></label>
	  	<select name="syear" class="form-control select2" id="syear" required="">
            <option value="">Admission For Academic Year</option>
            <option value="2001">2001</option>
            <option value="2020">2021</option>
        </select>
        </div>
        <div class="from-my">
        <label><h4>Student's Name</h4></label>
        <input type="text" class="form-control" placeholder="Student's Name" name="">
        </div>
        <div class="from-my">
        <label><h4>Father's Name</h4></label>
        <input type="text" class="form-control" placeholder="Father's Name" name="">
        </div>
        <div class="from-my">
        <label><h4>Mother's Name</h4></label>
        <input type="text" class="form-control" placeholder="Mother's Name" name="">
        </div>
        <div class="from-my">
        <label><h4>Date of Birth (In Figures)</h4></label>
        <input type="date" class="form-control" name="" placeholder="Date of Birth">
        </div>
         <div class="from-my">
        <label><h4>Gender</h4></label><br>
        <div class="main-gender">
        <input type="radio" name="gender" id="yes" value="male" checked>
        <label for="male">Male</label>
        <br>
        <input type="radio" name="gender" id="yes" value="female">
        <label for="female">Female</label>
        </div>
        </div>
	  </div>
	</div>
	<div class="column dt-sc-one-half">
	   <div class="form-register">
            <div class="from-my">
        <label><h4>Admission Sought to</h4></label>
        <select name="class" id="class" class="form-control">
           <option value="Class IV">Class IV</option>
            <option value="Class V">Class V</option>
            <option value="Class VI">Class VI</option>
            <option value="Class VII">Class VII</option>
            <option value="Class VIII">Class VIII</option>
            <option value="Class IX">Class IX</option>
            <option value="Class X">Class X</option>
            <option value="Class X">Class XI Science</option>
            <option value="Class X">Class XI Commerce</option>
            <option value="Class X">Class XII Science</option>
            <option value="Class X">Class XII Commerce</option>
        </select>
        </div>
	   	<div class="from-my">
        <label><h4>City</h4></label>
        <input type="text" class="form-control" name="" placeholder="City">
        </div>
        <div class="from-my">
        <label><h4>State</h4></label>
        <input type="text" class="form-control" name="" placeholder="State">
        </div>
        <div class="from-my">
        <label><h4>Mobile No</h4></label>
        <input type="text" class="form-control" name="" placeholder="Mobile No">
        </div>
        <div class="from-my">
        <label><h4>E-mail Address</h4></label>
        <input type="text" class="form-control" name="" placeholder="E-mail Address">
        </div>
	   </div> 
	</div>
	<div class="float-clear"></div>
	<div class="main-address">
		<div class="from-my">
        <label><h4>Residential Address</h4></label>
        <input type="text" class="form-control" name="" placeholder="Address">
        </div>
	</div>
	<div class="h4-main-bottom">
	<h4>Please recheck mobile number before submitting the form as you will receive a message carrying registration number on the same.</h4>
    </div>
    <div class="g-recaptcha" data-sitekey="your_site_key"></div>
    
    <div class="main-address">
	<input type="Submit" class="dt-sc-button small pink" name="Submit">	
	</div>
    </form>
  </div>
</div>
</section>
</div>
@endsection
