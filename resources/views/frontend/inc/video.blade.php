@extends('frontend.layout.master')
@section('contant')
<div id="main">
   <div class="breadcrumb-section">
      <div class="container">
         <h1>School Videos</h1>
         <div class="breadcrumb">
            <a href="index.html">Home</a>
            <span class="fa fa-angle-double-right"></span>
            <span class="current">School Video</span>
         </div>
      </div>
   </div>
   <div class="container">
      <section id="primary" class="content-full-width mainpicture">
         <div class="container">
            <div class="row">
               <div class="col-lg-4">
                  <div class="portfolio">
                     <div class="portfolio-thumb">
                        <div class="activity my-activiti box1">
                           <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                        </div>
                        <div class="image-overlay">
                           <a class="zoom myBtn"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                     <div class="modal">
                        <div class="modal-contents">
                           <span class="closemy">&times;</span>
                           <div class="embed-responsive embed-responsive-16by9 ">
                              <iframe class="embed-responsive-item" src="{{url('assets/Video/190828_27_SuperTrees_HD_17.mp4')}}" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="portfolio">
                     <div class="portfolio-thumb">
                        <div class="activity my-activiti box1">
                           <img src="{{url('assets/images/activity1.jpg')}}" alt="" title="">
                        </div>
                        <div class="image-overlay">
                           <a class="zoom myBtn"><span class="fa fa-search"></span></a>
                        </div>
                     </div>
                     <div class="modal">
                        <div class="modal-contents">
                           <span class="closemy">&times;</span>
                           <div class="embed-responsive embed-responsive-16by9 ">
                              <iframe class="embed-responsive-item" src="{{url('assets/Video/190828_27_SuperTrees_HD_17.mp4')}}" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </section>
   </div>
</div>
@endsection